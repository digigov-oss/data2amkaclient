import getAmka from '../src/index';
import config from './config.json'; 

const test = async () => {
    try {
        const data = await getAmka(
                                    config.surName, 
                                    config.firstName,
                                    config.fatherName,
                                    config.motherName,
                                    config.birthYear,
                                    config.afm,
                                    config.adt,
                                    config.user, config.pass);
        return data;
    } catch (error) {
        console.log(error);
    }
}

test().then((amka) => { console.log('data2amkaOutputRecord',amka); });