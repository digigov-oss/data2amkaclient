# data2amkaClient

Client to connect on IDIKA service, Returns AMKA among other personal info.

#### Example:

```
import getAmka from '@digigov-oss/data2amka-client';
import config from './config.json'; 

const test = async () => {
    try {
        const data = await getAmka(
                                    config.surName, 
                                    config.firstName,
                                    config.fatherName,
                                    config.motherName,
                                    config.birthYear,
                                    config.afm,
                                    config.adt,
                                    config.user, config.pass);
        return data;
    } catch (error) {
        console.log(error);
    }
}

test().then((amka) => { console.log('data2amkaOutputRecord',amka); });
```
* you can use `overrides` to override the default values
* for your tests, you don't need to use the `overrides` mecahnism, in that case, the default storage path will be used ie `/tmp`
* look at [KED](https://www.gsis.gr/dimosia-dioikisi/ked/) standard guides for records you can use on auditInit"
Also, you can use `overrides` to override the default storage engine. Look at the test folder, for example.

Look at module [AuditRecordDB](https://gitlab.grnet.gr/digigov-oss/auditRecordDB/-/blob/main/README.md) for more details on how to use the AuditEngine.

#### Returns
an object like the following:
```
{
  data2amkaOutputRecord: {
    aaInput: '7',
    surnameInput: 'ΣΚΑΡΒΕΛΗΣ',
    firstNameInput: 'ΠΑΝΑΓΙΩΤΗΣ',
    fatherNameInput: 'XXXXXXXXX',
    motherNameInput: 'XXXXXXXXX',
    birthDateInput: 'XXXXX',
    afmInput: 'XXXXXXXX',
    adtInput: 'XXXXXXXX',
    amkaCurrent: 'XXXXXXXXXXX',
    lastDataLogDate: '27/11/2017',
    adt: 'XXXXXXXXXX',
    nationalityCountry: 'ΕΛΛΑΔΑ',
    nationalityCountryCode: 'GR',
    afm: 'XXXXXXXXXXXX',
    birthSurnameEl: 'ΣΚΑΡΒΕΛΗΣ',
    idSurnameEl: 'ΣΚΑΡΒΕΛΗΣ',
    firstNameEl: 'ΠΑΝΑΓΙΩΤΗΣ',
    fatherNameEl: 'XXXXXXXXXX',
    motherNameEl: 'XXXXXXXXXX',
    birthSurnameEn: 'SKARVELIS',
    idSurnameEn: 'SKARVELIS',
    firstNameEn: 'PANAGIOTIS',
    fatherNameEn: 'XXXXXXXXXX',
    motherNameEn: 'XXXXXXXXX',
    birthDate: 'XX/XX/XXXXX'
  },
  callSequenceId: 48058817,
  callSequenceDate: 2022-02-10T14:54:08.234Z,
  errorRecord: null,
  auditUnit: 'gov.gr',
  auditTransactionId: '54',
  auditProtocol: '3/2022-02-11',
  auditTransactionDate: '2022-02-11T08:47:18Z',
  auditUserIp: '127.0.0.1',
  auditUserId: 'system'
}
```
or an error object like the following:
```
 {
  callSequenceId: 48141257,
  callSequenceDate: 2022-02-10T15:03:14.380Z,
  errorRecord: {
    errorCode: 'GEN_INVALID_DATA',
    errorDescr: 'Το πεδίο afm δεν είναι έγκυρος Αριθμός Φορολογικού Μητρώου'
  }
}
```

#### note
To test it, you have to use your real data.

#### * known issues
KED advertises a wrong endpoint(!) for the `data2amka` service on production WSDL. So, you have to use (override) the endpoint: `https://ked.gsis.gr/esb/amkaInfoService`
You can do that by setting the `endpoint` property on the `overrides` object.

```
const overrides = {
    endpoint: 'https://ked.gsis.gr/esb/amkaInfoService',
}
```